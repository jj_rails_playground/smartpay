FROM ruby:2.7.2
#FROM ruby:latest

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qq && apt-get install -y build-essential nodejs yarn

ENV APP_HOME /app
ENV RAILS_ENV development
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

RUN gem install bundler:2.1.2
ADD Gemfile* $APP_HOME/
RUN bundle config --global frozen 1
RUN bundle install

ADD . $APP_HOME
RUN yarn install --check-files
EXPOSE 5000
CMD ["rails", "server", "-p", "5000", "-b", "0.0.0.0"]